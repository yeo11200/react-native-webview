import React from 'react';
import {
  KakaoOAuthToken,
  KakaoProfile,
  getProfile as getKakaoProfile,
  login as kakaoLogin,
  logout,
  unlink,
  loginWithKakaoAccount
} from '@react-native-seoul/kakao-login';

const useKakaoLogin = () => {
  const [result, setResult] = React.useState('');

  const login = React.useCallback(() => {
    // login app을 연동하는 함수
    return kakaoLogin([KAKAO_AUTH_TYPES.Talk]).then(result => {

      console.log(result);

      // 프로필에 대한 정보가져오는 함수
      return getKakaoProfile().then(response => {
        
        console.log(response);
        return { id: response.id, nickname: response.nickname};
      })
    }).catch(e => {
      loginWithKakaoAccount();
      console.log('###################KAKALOGIN', e)
    });
  }, []);

  return { login, result };
};

export default useKakaoLogin;
