import React from "react";
import {
	Alert,
	SafeAreaView,
	StyleSheet,
	Button,
	Platform
} from "react-native";
import { NaverLogin, getProfile } from "@react-native-seoul/naver-login";

const iosKeys = {
	kConsumerKey: "naver client id",
	kConsumerSecret: "naver secret id",
	kServiceAppName: "테스트앱(iOS)",
	kServiceAppUrlScheme: "testapp" // only for iOS
};

const androidKeys = {
	kConsumerKey: "naver client id",
	kConsumerSecret: "naver secret id",
	kServiceAppName: "테스트앱(안드로이드)"
};

const initials = Platform.OS === "ios" ? iosKeys : androidKeys;

const useNaverLogin = () => {
	const [naverToken, setNaverToken] = React.useState(null);

	const naverLogin = () => {
		return NaverLogin.login(initials, async (err, token) => {
			console.log(`\n\n  Token is fetched  :: ${token} \n\n`);
			const profileResult = await getProfile(token.accessToken);

			if (profileResult.resultcode === "024") {
				Alert.alert("로그인 실패", profileResult.message);
				return;
			}

			const response = profileResult.response;
			return {id: response.id, nickname: response.name};
		});
	}

	const naverLogout = () => {
		NaverLogin.logout();
		setNaverToken("");
	};

	const getUserProfile = async () => {
		const profileResult = await getProfile(naverToken.accessToken);
		if (profileResult.resultcode === "024") {
			Alert.alert("로그인 실패", profileResult.message);
			return;
		}
		console.log("profileResult", profileResult);
	};

	return { naverLogin, getUserProfile}
};

export default useNaverLogin;