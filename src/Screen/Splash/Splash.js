import React from 'react';
import {Image, View} from "react-native";

const Splash = () => {
	return (
		<>
			<Image
				style={{height:'100%', width:'100%'}}
				source={{uri:'https://www.dooroo53.com/resources/client/images/splash.png'}}/>
		</>
	)
};

export default Splash;
