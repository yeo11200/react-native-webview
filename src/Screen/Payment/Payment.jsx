import React from 'react';
import IMP from 'iamport-react-native';

const Payment = ({navigation, route}) => {
	/* 가맹점 식별코드, 결제 데이터 추출 */
	const {useCode, data} = route.params;

	const callback = (response) => {
		const isSuccessed = getIsSuccessed(response);

		if (isSuccessed) {
			// 결제 성공한 경우, 리디렉션 위해 홈으로 이동한다
			const params = {
				response,
				type: 'payment',
			};
			navigation.replace('Home', params);
		}
		else {
			// 결제 실패한 경우, 본래 페이지로 돌아간다
			navigation.goBack();
		}
	}

	const getIsSuccessed = (response) => {
		const {imp_success, success} = response;

		if (typeof imp_success === 'string') {
			return imp_success === 'true';
		}
		if (typeof imp_success === 'boolean') {
			return imp_success === true;
		}
		if (typeof success === 'string') {
			return success === 'true';
		}
		if (typeof success === 'boolean') {
			return success === true;
		}
	}

	return (
		<IMP.Payment
			userCode={useCode}
			data={{
				...data,
				app_scheme: 'web',
			}}
			callback={callback}
		/>
	)
};

export default Payment;