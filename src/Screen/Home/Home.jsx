import React from 'react';
import {BackHandler, Platform} from 'react-native';
import WebView from 'react-native-webview';
import * as firebase from 'react-native-firebase';
import Splash from "./src/Screen/Splash/Splash";
import FCMContainer from "./src/custom/useLotifiMessage/messageHeandler";
import queryString from 'query-string';
import useNaverLogin from "../../custom/useNaverLogin";
import useKakaoLogin from "../../custom/useKakaoLogin";

const Home = ({navigation, route}) => {
	// Domain 주소
	const domain = 'https://www.dooroo53.com/';

	// URL이 바뀔 때 마다 탈 곳
	const [uri, setUri] = React.useState(domain);


	const webview = React.useRef({
		canGoBack : false
	});

	const os = Platform.OS;

	// 카카오 로그인
	const kakaoLogin = useKakaoLogin();

	// 네이버 로그인
	const naverLogin = useNaverLogin();

	const [isSplash, setIsSplash] = React.useState(false);

	/**
	 * 라우트 값이 변할 때 마다 탈 곳
	 */
	React.useEffect(() => {
		const type = route.params?.type;
		const response = route.params?.response;
		if (response) {
			// 결제와, 본인인증이 성공할 경우 reponse데이터로 담아서 하나의 제이슨객체를 문자열로 치환
			const query = queryString.stringify(response);

			// 결제
			if (type === 'payment') {
				setUri(`${domain}/payment/result?${query}`);
			}
			// 본인인증
			else {
				setUri(`${domain}/certification/result?${query}`);
			}
		}
	}, [route]);

	React.useEffect(() => {

		setTimeout(() => {
			setIsSplash(true);
		}, 2000);

		const unsubscribe = firebase.messaging().onMessage(async remoteMessage => {
			console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
		});

		return unsubscribe;

	}, []);

	const fnToken = async () => {
		return await firebase.messaging().getToken();
	}

	React.useEffect(() => {
		if (os === 'android') {
			BackHandler.addEventListener('hardwareBackPress', onAndroidBackPress);
		}
		return () => {
			BackHandler.removeEventListener('hardwareBackPress', onAndroidBackPress);
		};
	}, [os]); // Never re-run this effect

	const onAndroidBackPress = async () => {
		if (webview.canGoBack) {
			webview.current.goBack();
			return true; // prevent default behavior (exit app)
		}
		BackHandler.exitApp()
	};

	// url 여부
	const onShouldStartLoadWithRequest = (navigator) => {
		webview.canGoBack = navigator.canGoBack;
	}

	const webViewPostMessage = async (event) => {
		const value = JSON.parse(event.nativeEvent.data);

		if (value.status === 'kakao_login') {
			const info = await kakaoLogin.login();
			info.token = await fnToken();
			info.devType = 'APP';

			webview.current.postMessage(JSON.stringify(
				{ changeText: 'kakaoInfo', info : JSON.stringify(info)}
			),'*');
		}

		if (value.status === 'naver_login') {
			const info = await naverLogin.naverLogin();
			info.token = await fnToken();
			info.devType = 'APP';

			webview.current.postMessage(JSON.stringify(
				{ changeText: 'naverInfo', info : JSON.stringify(info)}
			),'*');
		}

		if (value.status === 'iam_port') {
			const {useCode, data, type} = JSON.parse(event.nativeEvent.data);
			const params = {useCode, data};
			navigation.push(type === 'payment' ? 'Payment' : 'Certification', params);
		}
	}

	return (
		<FCMContainer>
			{
				isSplash ?
					<WebView
						source={{uri}}
						// onShouldStartLoadWithRequest={onShouldStartLoadWithRequest} //for iOS
						onNavigationStateChange={onShouldStartLoadWithRequest}
						javaScriptEnabled={true}
						ref={webview}
						onMessage={webViewPostMessage}
					/>
					: <Splash />
			}
		</FCMContainer>
	);
};

export default Home;
