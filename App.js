import React from 'react';
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import {NavigationContainer} from "@react-navigation/native";
import Home from "./src/Screen/Home";
import Payment from "./src/Screen/Payment";
import Certification from "./src/Screen/Certification";

/**
 * 하나씩 쌓아가며 APP의 router를 정해준다.
 */
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Home'>
        <Stack.Screen name='Home' component={Home} />

        {/* 결제쪽 컴포넌트 */}
        <Stack.Screen name='Payment' component={Payment} />

        {/* 본인인증 컴포넌트 */}
        <Stack.Screen name='Certification' component={Certification} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;